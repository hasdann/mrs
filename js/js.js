$(document).foundation()

window.addEventListener("resize", destroySticky);

function destroySticky() {
	var widthWindow = $(window).width();
	if(widthWindow < 1025){
		$('#sticky').foundation('destroy');
	}
}

$('.hp__banner a').click(function(){
    	$('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    	}, 500);
    	return false;
});

new WOW().init();
